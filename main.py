import hashlib
import os
import secrets
import string
from functools import wraps
import datetime

import PyPDF2
import psycopg2 as psycopg2
from flask import Flask, request, jsonify
from minio import Minio
from pdf2image import convert_from_path

from minio.error import S3Error
from zipfile import ZipFile

import jwt

app = Flask(__name__)
UPLOAD_FOLDER = '/home/billion/Downloads'

app.config['SECRET_KEY'] = "this"

client = Minio("172.17.0.1:9000", access_key="minioadmin", secret_key="minioadmin", secure=False)
con = psycopg2.connect("host=172.17.0.1 dbname=minio_db user=postgres password=password")
cur = con.cursor()


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'})

        try:
            jwt_algorithm = 'HS256'
            data = jwt.decode(token, app.config['SECRET_KEY'], jwt_algorithm)
            current_user = data['user']
        except  Exception as e:

            print(f"Error message: {e}")
            return jsonify({'message': 'please login first'})

        return f(current_user, *args, **kwargs)

    return decorated


@app.route('/')
def index():
    return "hello user"


@app.route('/register', methods=["POST"])
def register():
    data = request.get_json()
    email = data['email']
    password = data['password']
    name = data['name']
    address = data['address']

    salt_value = salt_generator()
    new_pass = password + salt_value
    hashed = hashlib.sha256(new_pass.encode()).hexdigest()
    cur.execute(
        f"insert into user_data (email,password,salt,name,address) values ('{email}','{hashed}','{salt_value}','{name}'"
        f",'{address}')")
    con.commit()
    return "successfully"


@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    email = data['email']
    password = data['password']

    sql = f"select * from user_data where email='{email}'"

    cur.execute(sql)
    row = cur.fetchone()
    if row is not None:

        hashed = row[2]
        salt = row[3]

        user_pass = password + salt
        user_pass = hashlib.sha256(user_pass.encode()).hexdigest()

        if hashed == user_pass:
            token = jwt.encode({'user': email,
                                'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=120)},
                               app.config['SECRET_KEY'], algorithm="HS256")

            return jsonify({'token': token})
        else:
            return jsonify({'message': 'enter right password'})

    return jsonify({'message': 'enter right id'})


def salt_generator():
    string_source = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_lowercase)
    password += secrets.choice(string.ascii_uppercase)
    password += secrets.choice(string.digits)
    password += secrets.choice(string.punctuation)
    for _ in range(6):
        password += secrets.choice(string_source)
    char_list = list(password)
    secrets.SystemRandom().shuffle(char_list)
    password = ''.join(char_list)
    return password


@app.route('/loadfile')
@token_required
def loadfile(current_user):
    if not client.bucket_exists('test'):
        client.make_bucket('test')

    uploaded_file = request.files['file']
    file_name = uploaded_file.filename
    cur.execute(f"insert into upload_file (user_name,file_name) values('{current_user}','{file_name}')")
    con.commit()
    loc = os.path.join(UPLOAD_FOLDER, file_name)
    client.fput_object('test', file_name, loc)

    return "file upload successfully"


@app.route('/downloadfile')
@token_required
def downloadfile(current_user):
    try:
        client.fget_object('test1', '5104396884_APPRAISAL_151118.pdf', '/home/billion/Downloads/miniofile.pdf')
    except S3Error as e:
        print(e)
        return "bucket not exsist"

    return "done"


@app.route('/show')
@token_required
def showfile(current_user):
    cur.execute(f"select * from uploadfile where user_name='{current_user}'")
    data = cur.fetchall()
    information = []
    if data:
        for col in data:
            print(col[2])
            information.append(col[2])
    else:
        return "not upload file"

    return jsonify({'information': information})


@app.route('/split_pdf')
@token_required
def split_pdf(current_user):
    if not client.bucket_exists('test'):
        client.make_bucket('test')
    uploaded_file = request.files['file']
    file_name = uploaded_file.filename
    loc = os.path.join(UPLOAD_FOLDER, file_name)
    images = convert_from_path(loc)
    for i in range(len(images)):
        images[i].save('/home/billion/page' + str(i) + '.jpg', 'JPEG')
        '''with ZipFile('/home/billion/my_python_files.zip', 'a') as zipobject:
            zipobject.write('/home/billion/page' + str(i) + '.jpg')
    client.fput_object('test', file_name.rsplit('.')[0] + '.zip', '/home/billion/my_python_files.zip')'''
    return "split done"


@app.route('/content_pdf')
@token_required
def content_pdf(current_user):
    fileobject = request.files['file']
    readerobject = PyPDF2.PdfFileReader(fileobject)
    numberofpage = readerobject.numPages
    f = open("demofile2.txt", "w")
    for i in range(0, numberofpage):
        pageobj = readerobject.getPage(i)
        f.write(pageobj.extractText())

    f.close()
    fileobject.close()
    return "content extraction done"


@app.route('/merge_pdf')
@token_required
def merge_pdf(current_user, ):
    file1 = request.files['file1']
    file2 = request.files['file2']
    pdfs = [UPLOAD_FOLDER + '/' + file1.filename,
            UPLOAD_FOLDER + '/' + file2.filename]

    mergepdf = PyPDF2.PdfFileMerger()

    for pdf in pdfs:
        mergepdf.append(pdf)

    with open('/home/billion/Downloads/merge.pdf', 'wb') as f:
        mergepdf.write(f)
    return "PDF Merge successfully"


@app.route('/generate_pdf')
@token_required
def generate_pdf(current_user):
    f = request.files['file']
    numberofpages = request.form['numberofpages']

    readerobject = PyPDF2.PdfFileReader(f)
    numberofpages_inpdf = readerobject.numPages
    pdf_writer = PyPDF2.PdfFileWriter()
    if not numberofpages_inpdf >= numberofpages:
        print(f" In pdf number of  pages  {numberofpages_inpdf} but you enter {numberofpages}")
        numberofpages = numberofpages_inpdf
    for i in range(0, numberofpages):
        pageobj = readerobject.getPage(i)
        pdf_writer.addPage(pageobj)
    with open('file_gene.pdf', "wb") as output_file:
        pdf_writer.write(output_file)
    return "PDF generate successfully"


if __name__ == '__main__':
    app.run(debug=True)
